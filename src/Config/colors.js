export default {
  primary: '#F28821',
  secondary: '#28D8A1',
  success: '#396A30',
  danger: 'red',
  orange: '#F7CA4E',
  peach: '#FEF0DD',
  warning: '#F9D109',
  info: '#65B6E1',
  light: '#BBBBBB',
  dark: 'black',
  muted: '',
  white: 'white',
  blue:"#5468ff",
  blue2:"#62c3d0",
  cream:'#f0f0f0'
};