import AsyncStorage from '@react-native-async-storage/async-storage';

const api = "http://horoscopeandpalmistry.com/api/v1/"
var axios = require('axios');
var cookie = "__cfduid=dfbef4a007bcf3f4deb603d55e4cc91431615919642"
const getToken = async (data) => {
  var config = {
    method: 'post',
    url: api + 'init',
    headers: {
      'Cookie': cookie,
    },
    data: data
  };
  var response= await axios(config)
    return response.data
}
const horoscopeList = async (data, token) => {

  var config = {
    method: 'get',
    url: api + 'horoscope/list',
    headers: {
      'X-API-TOKEN': token,
      'Cookie': cookie,
    },
    data: data
  };

  var response= await axios(config)
  return response;
}
const horoscopeDetail =async (data, token) => {

  var config = {
    method: 'post',
    url: api + 'horoscope/detail',
    headers: {
      'X-API-TOKEN': token,
      'Cookie': cookie,
    },
    data: data
  };

  var response= await axios(config)
  return response;
}
export {
  getToken,
  horoscopeList,
  horoscopeDetail
}