import React, { Component } from 'react'
import {
    SafeAreaView,
    View,
    Keyboard,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    Alert,
    StyleSheet,
    Image,
    Text, Dimensions
} from 'react-native'
import { CommonActions } from '@react-navigation/native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { horoscopeDetail } from '../Services/Api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import HTML from 'react-native-render-html';

export default class HoroscopeDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            horoscopeDetail:[]
        };
      }
      async componentDidMount() {
        var FormData = require('form-data');
        var data = new FormData();
        data.append('id', (this.props.route.params.HoroscopeId).toString());
        var usertoken = await AsyncStorage.getItem("userToken")

        setTimeout(async ()=>{
        var response = await horoscopeDetail(data, usertoken)
        console.log("response",response.data.data)
            this.setState({ horoscopeDetail: response.data.data })}, 1000)
      }
    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={styles.keyboardeAvoidingStyle}
            >
                <SafeAreaView style={{ flex: 1, backgroundColor: '#171531' }}>
                    <ScrollView>
                        <View style={{ margin: 20 }}>

                            <Text style={{ color: "white", fontSize: 25, marginTop: 15, textAlign: "center" }}>Virgo</Text>
                            <View style={{ marginTop: 10, backgroundColor: "#32358e", height: 50, borderRadius: 20, alignItems: "center", flex: 1, flexDirection: "row" }}>
                                <TouchableOpacity style={{
                                    marginHorizontal: 15, width: (Dimensions.get("screen").width - 40) / 5
                                    , alignItems: "center"
                                }}>
                                    <Text style={{ color: "white" }}>DAY</Text>
                                </TouchableOpacity>
                                <View style={{ height: 50, width: (Dimensions.get("screen").width - 40) / 25, alignItems: "center", justifyContent: "center" }}>

                                    <View style={{ backgroundColor: "#171531", height: 50, width: 2 }}>
                                    </View>
                                </View>
                                <TouchableOpacity style={{ marginHorizontal: 25, width: (Dimensions.get("screen").width - 40) / 5, alignItems: "center" }}>
                                    <Text style={{ color: "white" }}>MONTH</Text>
                                </TouchableOpacity>
                                <View style={{ height: 50, width: (Dimensions.get("screen").width - 40) / 25, alignItems: "center", justifyContent: "center" }}>

                                    <View style={{ backgroundColor: "#171531", height: 50, width: 2 }}>
                                    </View>
                                </View>
                                <TouchableOpacity style={{ marginHorizontal: 15, width: (Dimensions.get("screen").width - 40) / 5, alignItems: "center" }}>
                                    <Text style={{ color: "green" }}>YEAR</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Image source={require("../Assets/Images/detail-day.png")}
                                    style={{ width: Dimensions.get("screen").width - 50, height: 200 }} />
                            </View>
                            <View style={{
                                height: "100%", backgroundColor: "#232048", width: Dimensions.get("screen").width - 50,
                                borderRadius: 15, marginTop: -10, padding: 20
                            }}>
                                    {this.state.horoscopeDetail.length>0?
      <HTML baseFontStyle={{fontSize:20, color:'white'}} source={{ html: this.state.horoscopeDetail[0].comment }} contentWidth={Dimensions.get("screen").width} />:null
                                    }
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </KeyboardAvoidingView>
        )
    }
}
const styles = StyleSheet.create({
    keyboardeAvoidingStyle: {
        flex: 1
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#171531'
    },
    logoImgStyle: {
        flex: 1,
        marginTop: 90,
        marginBottom: 30,
        marginLeft: 110,
        height: 170,
        width: 200
    },
});
