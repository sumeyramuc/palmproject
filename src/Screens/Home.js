import React, { Component, useEffect, useState } from 'react'
import { 
    View, 
    FlatList, 
    ActivityIndicator, 
    StyleSheet ,
    SafeAreaView,
    Text,
    StatusBar,
    Dimensions,
    Image,
    TextInput
} from 'react-native'
import { CommonActions } from '@react-navigation/native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'


export default class Home extends Component{
    
     searchNavigate() {
        props.navigation.dispatch(
            CommonActions.navigate({
                name: 'Search',
            })
        )
    }
        render() {
    return (
        loading ? 
          <View style={styles.loading}>
          <ActivityIndicator size="large" color="gray"/>
          <Text>Loading...</Text>
          <StatusBar barStyle="default"/>
        </View>
            :
            <SafeAreaView style={{ flex: 1 , backgroundColor:'#32354e'}}>
            <ScrollView>
                <Text>dsfghj</Text>
            </ScrollView>
            </SafeAreaView>
    )
}
}
const styles = StyleSheet.create({
   container: {
        flex: 1,
        backgroundColor: '#32354e'
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
      },
      searchContainer: {
        width: Dimensions.get('window').width / 1,
        backgroundColor: '#757aa5',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    image: {
        marginLeft: 35,
        height:80,
        width:50,
        marginRight:25
    },
    textInput: {
        flex: 1,
        fontWeight: 'bold',
        marginHorizontal: 10,
        fontSize: 16
    }
});


export { Home }