import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    Dimensions, Image, Button, TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { withOrientation } from 'react-navigation';
export default class Analysis extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.boxStyle} >
                <Image source={require("../Assets/Images/avuc.jpeg")}style={styles.imgStyle} resizeMode="cover"></Image>
               
               <View style={{display: "flex" , alignItems: "flex-start", padding: 15}}>
                    <Text style={{fontSize: 23, fontWeight: "bold", color: "#fff" }}>Mike</Text>
                    <Text style={{fontSize: 13, fontWeight: "bold", color: "#fff", opacity: 0.5  }}>2 days ago</Text>
                    <Text style={{alignSelf: "flex-end", marginTop: -40, backgroundColor: "#32354e", padding: 10, borderRadius: 20, overflow: "hidden"}}><Icon name="arrow-right"  size={15} color="gray"></Icon></Text>
                    
                </View>
                </View> 
                <View style={styles.boxStyle} >
                <Image source={require("../Assets/Images/avuc.jpeg")}style={styles.imgStyle} resizeMode="cover"></Image>
               
               <View style={{display: "flex" , alignItems: "flex-start", padding: 15}}>
                    <Text style={{fontSize: 23, fontWeight: "bold", color: "#fff" }}>Mike</Text>
                    <Text style={{fontSize: 13, fontWeight: "bold", color: "#fff", opacity: 0.5  }}>2 days ago</Text>
                    <Text style={{alignSelf: "flex-end", marginTop: -40, backgroundColor: "#32354e", padding: 10, borderRadius: 20, overflow: "hidden"}}><Icon name="arrow-right"  size={15} color="gray"></Icon></Text>
                    
                </View>
                </View> 
            </View>
          
            

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#32354e',
        alignItems:"center",
        justifyContent: "flex-start",
        paddingTop: 125
    },
    boxStyle: {
        width: "80%",  
        height: "35%",  
        overflow: "hidden",
        backgroundColor: "#301b3f",
        borderRadius: 15,
        marginBottom: 15,
    },
    imgStyle: {
        width: "100%",
        height: "70%",
        overflow: "hidden",
    },
    textStyle: {
        fontSize: 35,
        color: "white",
        backgroundColor: "#2978b5",
        paddingVertical: 5,
        paddingHorizontal: 95,
        overflow: "hidden",
        borderRadius: 10,
    }
});


export { Analysis }