import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    Dimensions, Image, Button, TouchableOpacity,Switch
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
export default class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isEnabled: false
        };
      }
   toggleSwitch = () =>{
       this.setState({isEnabled:!this.state.isEnabled})
   }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.btnBox}>
                    <View style={styles.Icon}><Icon name="bell" size={20} color="#fff"></Icon></View>

                    <TouchableOpacity>
                        <Text style={{fontSize: 25, color: "#fff", fontWeight: "600"}}>Notification</Text>
                      </TouchableOpacity>
                      <Switch
        trackColor={{ false: "#767577", true: "#72efdd" }}
        thumbColor={this.state.isEnabled ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={this.toggleSwitch.bind(this)}
        value={this.state.isEnabled}
      />
                </View>
                <View style={{ width: "100%", backgroundColor: "#301b3f", borderRadius: 15,}}>
                <TouchableOpacity style={styles.btn}>
                <View style={styles.Icon}><Icon name="question"  size={20} color="#fff"></Icon></View>
                <Text style={{fontSize: 25, color: "#fff"}}>Help Center</Text>
                <Text style={{color: "gray", fontSize: 20}}>></Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('Privacy')}>
                <View style={styles.Icon}><Icon name="shield"  size={20} color="#fff"></Icon></View>
                <Text style={{fontSize: 25, color: "#fff"}}>Privacy & Terms</Text>
                <Text style={{color: "gray", fontSize: 20}}>></Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn}>
                <View style={styles.Icon}><Icon name="envelope"  size={20} color="#fff"></Icon></View>
                <Text style={{fontSize: 25, color: "#fff"}}>Contact Us</Text>
                <Text style={{color: "gray", fontSize: 20}}>></Text>
                </TouchableOpacity>
                </View> 

                
            </View>
          
            

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#32354e',
        alignItems:"center",
        justifyContent: "flex-start",
        paddingTop: 100,
        paddingHorizontal: 25
    },
    Icon: {
    width: 45,
    height: 45, 
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#3d405b",
    borderRadius: 50,
    },
    btnBox: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "#301b3f",
        width: "100%",
        padding: 20,
        borderRadius: 15,
        margin: 25,
    },
    btn: {
        width: "100%",  
        padding: 20,
        flexDirection: "row",
        justifyContent: "space-between",    
    }
});


export { Settings }