import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    Dimensions, Image, Button, TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { withOrientation } from 'react-navigation';
export default class Palmistry extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.boxStyle} >
                <Image source={require("../Assets/Images/palmistry.png")}style={styles.imgStyle} resizeMode="cover"></Image>
               
               <View style={{display: "flex" , alignItems: "flex-start", padding: 15}}>
                    <Text style={{fontSize: 23, fontWeight: "bold", color: "#fff" }}>Scan Your Palm </Text>
                    <Text style={{fontSize: 13, fontWeight: "bold", color: "#fff", opacity: 0.5  }}>We will analyze palm lines</Text>
                    
                </View>
                </View> 
                <View style={styles.boxStyle} >
                <Image source={require("../Assets/Images/palmistryy.png")}style={styles.imgStyle} resizeMode="cover"></Image>
               
               <View style={{display: "flex" , alignItems: "flex-start", padding: 15}}>
                    <Text style={{fontSize: 23, fontWeight: "bold", color: "#fff" }}>Palmistry Analysis</Text>
                    <Text style={{fontSize: 13, fontWeight: "bold", color: "#fff", opacity: 0.5  }}>Our analysis on palmistry</Text>

                    
                </View>
                </View> 
            </View>
          
            

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#32354e',
        alignItems:"center",
        justifyContent: "flex-start",
        paddingTop: 125
    },
    boxStyle: {
        width: "90%",  
        height: "35%",  
        overflow: "hidden",
        backgroundColor: "#301b3f",
        borderRadius: 15,
        marginBottom: 20,
    },
    imgStyle: {
        width: "100%",
        height: "70%",
        overflow: "hidden",
    },
    textStyle: {
        fontSize: 35,
        color: "white",
        backgroundColor: "#2978b5",
        paddingVertical: 5,
        paddingHorizontal: 95,
        overflow: "hidden",
        borderRadius: 10,
    }
});


export { Palmistry }