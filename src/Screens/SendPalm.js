import React, { Component } from 'react'
import { 
    SafeAreaView, 
    View, 
    Keyboard, 
    TouchableWithoutFeedback, 
    KeyboardAvoidingView, 
    Alert, 
    StyleSheet,
    Image,
    Text, 
    Dimensions,
    TextInput
} from 'react-native'
import { CommonActions } from '@react-navigation/native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Button from "../Components/Button";
import colors from '../Config/colors';
const SendPalm = ({ navigation }) => {

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : null}
            style={styles.keyboardeAvoidingStyle}
        >
            <SafeAreaView style={{ flex: 1 , backgroundColor:'#171531'}}>
                <ScrollView>
                    <View style={{margin:20}}>

                    <View style={{marginTop:50,alignItems:"center",justifyContent:"center"}}>
                    <Image source={require("../Assets/Images/hand_vector.png")}
                   style={{ width:100, height: 100}}/>
                    </View>
                        <View style={{marginTop:20}}>
                            <TextInput style={{backgroundColor:'#232048',borderRadius:15,padding:10,height:60,marginTop:15}} placeholder="Your Name" placeholderTextColor="#ccc"></TextInput>
                        </View>
                    <View style={{flex:1,flexDirection:"row",margin:10}}>
                    <TouchableOpacity
                     style={{backgroundColor:'#5468ff',height:55,
                     width:Dimensions.get("screen").width/4,borderRadius:15,alignItems:"center",justifyContent:"center",marginTop:15}}>
                        <Text style={{color:"white"}}>Female</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                     style={{backgroundColor:'#5468ff',height:55,marginHorizontal:20,
                     width:Dimensions.get("screen").width/4,borderRadius:15,alignItems:"center",justifyContent:"center",marginTop:15}}>
                        <Text style={{color:"white"}}>Male</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                     style={{backgroundColor:'#5468ff',height:55,
                     width:Dimensions.get("screen").width/4,borderRadius:15,alignItems:"center",justifyContent:"center",marginTop:15}}>
                        <Text style={{color:"white"}}>None</Text>
                    </TouchableOpacity>
                    </View>
                            <TextInput style={{backgroundColor:'#232048',borderRadius:15,padding:10,height:60,marginTop:15}} placeholder="Single"  placeholderTextColor="#ccc"></TextInput>
                            <TextInput style={{backgroundColor:'#232048',borderRadius:15,padding:10,height:60,marginTop:15}} placeholder="Date of birth" placeholderTextColor="#ccc"></TextInput>
                     
                            <TextInput style={{backgroundColor:'#232048',borderRadius:15,padding:10,height:60,marginTop:15}} placeholder="Time of birth" placeholderTextColor="#ccc"></TextInput>
                     
                            <TextInput style={{backgroundColor:'#232048',borderRadius:15,padding:10,height:60,marginTop:15}} placeholder="City you were born" placeholderTextColor="#ccc"></TextInput>
                    <View style={{marginTop:10}}>
                    <Button
                onPress={()=>navigation.navigate('Preparing')}
                style={{
                    backgroundColor: colors.blue,
                    marginRight: 10,
                    marginTop: 25,
                }}
                buttonText={"SEND"}
                  />
                    </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </KeyboardAvoidingView>
    )

}
const styles = StyleSheet.create({
    keyboardeAvoidingStyle: {
        flex: 1
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#171531'
    },
    logoImgStyle: {
        flex: 1,
        marginTop: 90,
        marginBottom: 30,
        marginLeft:110,
        height:170,
        width:200
      },
});
export { SendPalm }