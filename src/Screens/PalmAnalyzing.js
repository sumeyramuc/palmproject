import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    Dimensions, Image, Button, TouchableOpacity, ScrollView, ActivityIndicator, KeyboardAvoidingView,
    StatusBar
} from 'react-native'
import { withOrientation } from 'react-navigation';
export default class PalmAnalyzing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timer: false,
            loading:true
        };
    }
    componentDidMount() {
        setTimeout(() => {
            this.setState({ timer: true,loading:false })
        }, 3000)


    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="transparent" translucent={true}/>

                <View style={{}} >
                    <Image source={require("../Assets/Images/hand.png")} style={styles.imgStyle} resizeMode="contain"></Image>
                    <View style={{ position: "absolute", margin: 55 }}>
                        <Text style={{ fontSize: 21, textAlign: "center", lineHeight: 25 }}>Your Palm Analysis</Text>
                        <Text style={{ fontSize: 18, textAlign: "center", lineHeight: 25 }}>We willa analyze paim lines consisting of heart line, head line and life line</Text>
                    </View>
                </View>

                <ScrollView style={{marginTop:-150}}>
                <View style={{ backgroundColor: "#301b3f", padding: 15, marginHorizontal: 30, borderRadius: 15, height: 500 }}>
                        <View style={{ alignItems: "center",}}>

                            <Image source={require("../Assets/Images/Onboarding.png")} style={{ width: 100, height: 100, borderRadius: 100 / 2, marginVertical: 15 }} resizeMode="contain"></Image>
                            <ActivityIndicator size="small" color="#fff" animating={this.state.loading}/>
                           {this.state.timer?
                                  <Text style={{ color: "white", textAlign: "center", marginHorizontal: 25 }}><Text style={{ color: "white", fontSize: 25 }}>Analysis{"\n"}</Text>Why do we use it?
                                  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</Text>
                                  :                            <Text style={{ color: "white", textAlign: "center", marginHorizontal: 25 }}><Text style={{ color: "white", fontSize: 25 }}>Analiz{"\n"}</Text>Your palmistry is currently being interpreted.We will inform you as soon as possible.</Text>
                                }
                            
                     
                </View>
                </View>
                        </ScrollView>
            </View>



        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#32354e',
        alignItems: "center",
        justifyContent: "flex-start",
    },
    imgStyle: {
        overflow: "hidden",
        width: Dimensions.get("screen").width,
    },
    textStyle: {
        fontSize: 35,
        color: "white",
        backgroundColor: "#2978b5",
        paddingVertical: 5,
        paddingHorizontal: 95,
        overflow: "hidden",
        borderRadius: 10,
    }
});


export { PalmAnalyzing }