import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    Dimensions, Image, Button, TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { withOrientation } from 'react-navigation';
export default class Notifications extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{height:140, weight:140, borderRadius: 140 / 2,  }} >
                <Image source={require("../Assets/Images/Onboarding.png")}style={styles.imgStyle} ></Image>
                </View> 
                
                <Text style={{color:"white", textAlign:"center",marginHorizontal:25, marginTop: 70}}><Text style={{color:"white",fontSize:25, lineHeight: 55}}>Notificationss{"\n"}</Text>Stay notified about how your personal and global factors influence.</Text>
                <View style={{marginHorizontal:40,width: "50%"}}>
                <TouchableOpacity style={[styles.btnStyle]}>
                    <Text style={{width:"85%",textAlign:"center",marginLeft:20,color:"white"}}>ALLOW</Text>
                    <View style={{width:30,height:30,borderRadius:30/2,backgroundColor:"gray",alignItems:"center",justifyContent:"center",marginRight:30,}}>
                        <Icon name="arrow-right" size={15} color="white"></Icon>
                    </View>
                </TouchableOpacity>
                </View>
                <View style={{width:"100%"}}>
                <TouchableOpacity style={styles.skipStyle} onPress={() => this.props.navigation.navigate('PalmAnalyzing')}>
                    <Text style={{color: "#eee"}} >SKIP</Text>
                </TouchableOpacity>
                </View>
            </View>
          
            

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#32354e',
        alignItems:"center",
        justifyContent: "space-around",
        paddingTop: 220,
    },
    imgStyle: {
        height: 230,
        width: 230,
        overflow: "hidden",
        borderRadius: 230 / 2,
    },
    btnStyle: {
        alignItems:"center",
        justifyContent:"center",
        backgroundColor: "#2978b5",
        borderRadius: 10,
        flexDirection: "row",
        height:50,
    },
    skipStyle: {
        alignItems: "center",
        justifyContent: "center",
    }
});


