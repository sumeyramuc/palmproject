import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    Dimensions, Image, Button, TouchableOpacity
} from 'react-native'
import { withOrientation } from 'react-navigation';
export default class Analyzing extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{height:140, weight:140, borderRadius: 140 / 2,  }} >
                <Image source={require("../Assets/Images/Onboarding.png")}style={styles.imgStyle} resizeMode="contain"></Image>
                </View> 
                
                <Text style={{color:"white", textAlign:"center",marginHorizontal:25}}><Text style={{color:"white",fontSize:25}}>Analiz{"\n"}</Text>Your palmistry will be interpreted by our experts within <Text style={{fontWeight: "bold", color: "#5aa897"}}>15 minutes</Text>at the latest.</Text>
                <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifications')}>
                    <Text style={styles.textStyle}>Tamam</Text>
                </TouchableOpacity>
            </View>
            </View>
          
            

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#32354e',
        alignItems:"center",
        justifyContent: "space-around",
        paddingTop: 220,
    },
    imgStyle: {
        height: 230,
        width: 230,
        overflow: "hidden",
        borderRadius: 230 / 2,
    },
    textStyle: {
        fontSize: 35,
        color: "white",
        backgroundColor: "#2978b5",
        paddingVertical: 5,
        paddingHorizontal: 95,
        overflow: "hidden",
        borderRadius: 10,
    }
});


export { Analyzing }