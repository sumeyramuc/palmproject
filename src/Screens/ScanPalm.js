import React, { Component } from 'react'
import { 
    SafeAreaView, 
    View, 
    Keyboard, 
    TouchableWithoutFeedback, 
    KeyboardAvoidingView, 
    Alert, 
    StyleSheet,
    Image,
    Text, 
    Dimensions
} from 'react-native'
import { CommonActions } from '@react-navigation/native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Button from "../Components/Button";
import colors from '../Config/colors';

const ScanPalm = ({ navigation }) => {

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : null}
            style={styles.keyboardeAvoidingStyle}
        >
            <SafeAreaView style={{ flex: 1 , backgroundColor:'#171531'}}>
                <ScrollView>
                    <View style={{margin:20}}>

                    <Text style={{marginHorizontal:30,color:"white", fontSize:18,marginTop:15,textAlign:"center"}}>
                        We will analyze palm lines consisting of heart line,head line and life line
                    </Text>
       
                    <View style={{marginTop:50}}>
                    <Image source={require("../Assets/Images/hand_vector.png")}
                   style={{ width:Dimensions.get("screen").width-50, height: 400}}/>
                    </View>
                        <Text style={{color:"gray",textAlign:"center",marginTop:30}}>No biometric data ...</Text>
                    
                    <View style={{marginTop:10}}>
                    <TouchableOpacity onPress={()=>{navigation.navigate("OpenCamera")}}
                     style={{backgroundColor: colors.blue,height:45,borderRadius:15,alignItems:"center",justifyContent:"center"}}>
                        <Text style={{color:"white"}}>CONTINUE</Text>
                    </TouchableOpacity>
                    </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </KeyboardAvoidingView>
    )

}
const styles = StyleSheet.create({
    keyboardeAvoidingStyle: {
        flex: 1
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#171531'
    },
    logoImgStyle: {
        flex: 1,
        marginTop: 90,
        marginBottom: 30,
        marginLeft:110,
        height:170,
        width:200
      },
});
export { ScanPalm }