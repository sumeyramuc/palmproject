import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { Component } from 'react'
import {
  SafeAreaView,
  View,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Alert,
  StyleSheet,
  Image,
  Text,
  Dimensions,
  FlatList
} from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
const boxSize = (Dimensions.get("screen").width - 60) / 2.1;
const screenHeight = Dimensions.get("screen").height;
import Api, { horoscopeList } from "../Services/Api";

export default class Horoscope extends Component {
  constructor(props) {
    super(props);
    this.state = {
      horoscopeList: []
    };
  }
  async componentDidMount() {
    var FormData = require('form-data');
    var data = new FormData();
    var usertoken = await AsyncStorage.getItem("userToken")
    var response = await horoscopeList(data, usertoken)
    console.log("response",response.data.data)
    this.setState({ horoscopeList: response.data.data })
  }
  getImage(id) {
    console.log("id",id)
    switch (id) {
      case 1:
        return require("../Assets/Icons/capricorn.png")
      case 2:
        return require("../Assets/Icons/leo.png")
      case 3:
        return require("../Assets/Icons/Sagittarius.png")
      case 4:
        return require("../Assets/Icons/taurus.png")
      case 5:
        return require("../Assets/Icons/virgo.png")
      case 6:
        return require("../Assets/Icons/capricorn.png")
      case 7:
        return require("../Assets/Icons/gemini.png")
      case 8:
        return require("../Assets/Icons/libra.png")
      case 9:
        return require("../Assets/Icons/Aquarius.png")
      case 10:
        return require("../Assets/Icons/cancer.png")
      case 11:
        return require("../Assets/Icons/scorpio.png")
      case 12:
        return require("../Assets/Icons/pisces.png")
        default:
        return ""
    }
  }
  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : null}
        style={styles.keyboardeAvoidingStyle}
      >
        <SafeAreaView style={{ flex: 1, backgroundColor: '#171531' }}>
          <ScrollView>
            <View
              style={{
                marginTop: screenHeight - screenHeight * 0.90,
                margin: 20,
                backgroundColor: "transparent",
              }}
            >
              <Text style={styles.titleStyle}>Horoscope</Text>
              <View style={{ flexDirection: "row" }}>
                <FlatList
                  columnWrapperStyle={{ justifyContent: 'space-between' }}
                  data={this.state.horoscopeList}
                  keyExtractor={item => item.id}
                  numColumns={2}
                  renderItem={({ item }) => {
                    return (
                      <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('HoroscopeDetail',{HoroscopeId:item.id})}
                        style={[styles.boxStyle, { marginBottom: 20 }]}
                      >
                        <Image source={this.getImage(item.id)}
                          style={styles.imageStyle} />
                        <Text style={styles.buttonTitleStyle}>{item.name}</Text>
                        <Text style={styles.buttonTitleStyle}>Dec 21 - Jan 20</Text>
                      </TouchableOpacity>
                    )
                  }}
                />
                {/*
           <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={[styles.boxStyle, { marginLeft: 30 }]}
                >
                  <Image source={require("../Assets/Icons/Aquarius.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Aquarius</Text>
                  <Text style={styles.buttonTitleStyle}>Jan 21 - Feb 19</Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: "row", marginTop: 20 }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={styles.boxStyle}
                >
                  <Image source={require("../Assets/Icons/pisces.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Pisces</Text>
                  <Text style={styles.buttonTitleStyle}>Feb 20 - Mar 20</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={[styles.boxStyle, { marginLeft: 30 }]}
                >
                  <Image source={require("../Assets/Icons/capricorn.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Aries</Text>
                  <Text style={styles.buttonTitleStyle}>Mar 21 - Apr 19</Text>
                </TouchableOpacity>

              </View>
              <View style={{ flexDirection: "row", marginTop: 20 }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={styles.boxStyle}
                >
                  <Image source={require("../Assets/Icons/taurus.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Taurus</Text>
                  <Text style={styles.buttonTitleStyle}>Apr 20 - May 20</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={[styles.boxStyle, { marginLeft: 30 }]}
                >
                  <Image source={require("../Assets/Icons/gemini.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Gemini</Text>
                  <Text style={styles.buttonTitleStyle}>May 21 - Jun 21</Text>
                </TouchableOpacity>

              </View>
              <View style={{ flexDirection: "row", marginTop: 20 }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={styles.boxStyle}
                >
                  <Image source={require("../Assets/Icons/cancer.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Cancer</Text>
                  <Text style={styles.buttonTitleStyle}>Jun 22 - Jul 23</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={[styles.boxStyle, { marginLeft: 30 }]}
                >
                  <Image source={require("../Assets/Icons/leo.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Leo</Text>
                  <Text style={styles.buttonTitleStyle}>Jul 24 - Aug 23</Text>
                </TouchableOpacity>

              </View>
              <View style={{ flexDirection: "row", marginTop: 20 }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={styles.boxStyle}
                >
                  <Image source={require("../Assets/Icons/virgo.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Virgo</Text>
                  <Text style={styles.buttonTitleStyle}>Apr 20 - May 20</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={[styles.boxStyle, { marginLeft: 30 }]}
                >
                  <Image source={require("../Assets/Icons/libra.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Libra</Text>
                  <Text style={styles.buttonTitleStyle}>May 21 - Jun 21</Text>
                </TouchableOpacity>

              </View>
              <View style={{ flexDirection: "row", marginTop: 20 }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={styles.boxStyle}
                >
                  <Image source={require("../Assets/Icons/scorpio.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Scorpio</Text>
                  <Text style={styles.buttonTitleStyle}>Oct 23 - Nov 21</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('HoroscopeDetail')}
                  style={[styles.boxStyle, { marginLeft: 30 }]}
                >
                  <Image source={require("../Assets/Icons/Sagittarius.png")}
                    style={styles.imageStyle} />
                  <Text style={styles.buttonTitleStyle}>Sagittarius</Text>
                  <Text style={styles.buttonTitleStyle}>Nov 22 - Dec 21</Text>
                </TouchableOpacity>
                */}

              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  keyboardeAvoidingStyle: {
    flex: 1
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#171531'
  },
  logoImgStyle: {
    flex: 1,
    marginTop: 90,
    marginBottom: 30,
    marginLeft: 110,
    height: 170,
    width: 200
  },
  boxStyle: {
    width: boxSize,
    height: boxSize - 20,
    borderRadius: 15,
    elevation: 2,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#232048",
    marginLeft: 5
  },
  buttonTitleStyle: {
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "center",
    color: "#5b849a",
    paddingTop: 10,
  },
  imageStyle: {
    height: 75,
    width: 70
  },
  titleStyle: {
    fontSize: 25,
    color: '#ffff',
    fontStyle: 'normal',
    marginLeft: 130,
    marginBottom: 40,
    marginTop: -50
  }
});

export { Horoscope }