import React, { Component, useState } from 'react'
import {
    SafeAreaView,
    View,
    Keyboard,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    Alert,
    StyleSheet,
    Image,
    Text, Dimensions
} from 'react-native'
import { CommonActions } from '@react-navigation/native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Button from "../Components/Button";
import colors from '../Config/colors';
import { getToken } from "../Services/Api";
import AsyncStorage from '@react-native-async-storage/async-storage';
export default class Onboarding extends Component {
    async componentDidMount() {
        // var api= Api.createToken();
        // var data= await api.getToken();
        var FormData = require('form-data');
        var data = new FormData();
        data.append('uuid', '19172');
        data.append('locale', 'tr');
        data.append('os', '1');
        data.append('appVersion', '1.0');
        data.append('osVersion', '1.0');
        data.append('region', 'turkey');
        var response =await getToken(data)
        await AsyncStorage.setItem("userToken",response.data.user.token)
        
    }
    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={styles.keyboardeAvoidingStyle}
            >
                <SafeAreaView style={{ flex: 1, backgroundColor: '#171531' }}>
                    <ScrollView>
                        <View style={{margin:20,flex:1}}>
                        <View style={{height:"50%",alignItems:"center"}}>

                        <Image source={require("../Assets/Images/Onboarding.png")}
                            style={{ width: 450, height: 350 }} resizeMode="contain"/>
                        </View>
                        <View style={{height:"35%",marginTop:50}}>
                        <Text style={styles.text}>Hello</Text>
                        <Text style={[styles.text,{color:"#ccc"}]}>It’s pleasure to meet you</Text>
                        <Button
                            onPress={() => this.props.navigation.navigate('Palmistry')}
                            style={{
                                backgroundColor: colors.blue,
                                marginRight: 10,
                                marginTop: 25,
                            }}
                            buttonText={"Palmmistry"}
                        />
                        <Button
                            onPress={() => this.props.navigation.navigate('Settings')}
                            style={{
                                backgroundColor: colors.blue,
                                marginRight: 10,
                                marginTop: 25,
                            }}
                            buttonText={"Settings"}
                        />
                        </View>
                        <View style={{}}>

                        <Text style={{textAlign:"center",color:"gray"}}>The data you submit will be processed in accordance with our
                        <Text style={styles.text2}> Privacy Notice</Text>. By clicking continuing you are agreeing to terms
                and condition in our <Text style={styles.text2}>EULA</Text></Text>

                
                        <Button
                            onPress={() => this.props.navigation.navigate('Horoscope')}
                            style={{
                                backgroundColor: colors.blue,
                                marginRight: 10,
                                marginTop: 25,
                            }}
                            buttonText={"GET STARTED"}
                        />
                        </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </KeyboardAvoidingView>
        )
    }
}
const styles = StyleSheet.create({
    keyboardeAvoidingStyle: {
        flex: 1
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#32354e'
    },
    logoImgStyle: {
        flex: 1,
        marginTop: 90,
        marginBottom: 30,
        marginLeft: 110,
        height: 170,
        width: 200
    },
    text:{textAlign:"center",color:"#fff",fontSize:20,lineHeight:30},
    text2:{color:colors.blue,textDecorationLine:"underline"}
});

export { Onboarding }