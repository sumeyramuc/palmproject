import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    Dimensions, Image, Button, TouchableOpacity,Switch, ScrollView
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
export default class Privacy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isEnabled: false
        };
      }
   toggleSwitch = () =>{
       this.setState({isEnabled:!this.state.isEnabled})
   }
    render() {
        return (
            <View style={styles.container}>
               
                <View style={{padding: 20  , width: "100%", height: "100%" ,backgroundColor: "#301b3f", borderRadius: 15,}}>
                <ScrollView>
                <Text style={styles.text}>
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                </Text>
                </ScrollView>
                </View> 

                
            </View>
          
            

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#32354e',
        alignItems:"center",
        justifyContent: "flex-start",
        paddingTop: 100,
        paddingHorizontal: 25
    },
    text:{
        color:"#ccc",
        lineHeight:23,
        fontSize:17
    }
});


export { Privacy }