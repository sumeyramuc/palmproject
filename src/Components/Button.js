import React from 'react';
import ReactNative from 'react-native';
import colors from '../Config/colors';
import {Text} from 'react-native';

// eslint-disable-next-line object-curly-newline
const Button = ({ style, isLoading, buttonText, theme, ...rest }) => (
  <ReactNative.TouchableOpacity
    style={[styles(theme).button, style]}
    disabled={isLoading}
    {...rest}
  >
    <ReactNative.View style={{ height:48, justifyContent: 'center', alignItems: 'center' }}>
      {isLoading
        ? <ReactNative.ActivityIndicator color={colors.white}/>
        : <Text style={styles(theme).buttonText}>{buttonText}</Text>}
    </ReactNative.View>
  </ReactNative.TouchableOpacity>
);

export default Button;

const styles = theme => ReactNative.StyleSheet.create({
  button: {
    shadowColor: theme === 'Login' ? null : 'black',
    shadowOffset: theme === 'Login' ? null : { width: 0, height: 2 },
    shadowOpacity: theme === 'Login' ? null : 0.2,
    shadowRadius: theme === 'Login' ? null : 1,
    backgroundColor: colors.blue,
    padding: theme === 'Login' ? 15 : 10,
    borderRadius: 15,
    marginBottom: 10
  },
  buttonText: {
    color: 'white',
    fontWeight: theme === 'Login' ? '500' : 'bold',
    fontSize: 25
  }
});
