import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Onboarding, Home, Horoscope } from './Screens'
import HoroscopeDetail from './Screens/HoroscopeDetail';
import OpenCamera from './Screens/OpenCamera';
import { Button } from 'react-native';
import { ScanPalm } from './Screens/ScanPalm';
import { SendPalm } from './Screens/SendPalm';
import { Preparing } from './Screens/Preparing';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Analyzing from './Screens/Analyzing';
import Notifications from './Screens/Notifications';
import PalmAnalyzing from './Screens/PalmAnalyzing';
import Analysis from './Screens/Analysis';
import { NavigationActions } from 'react-navigation';
import Palmistry from './Screens/Palmistry';
import Settings from './Screens/Settings';
import Privacy from './Screens/Privacy';

const Stack = createStackNavigator()

function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Onboarding" component={Onboarding} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        <Stack.Screen name="Horoscope" component={Horoscope} options={{
          headerShown: true, title: 'Horoscope',
          headerStyle: {
            backgroundColor: '#171531', elevation: 0,
            shadowColor: 'transparent'
          },
          headerTitleStyle: { color: '#5468ff' },
          headerTintColor: { color: '#5468ff' },
          headerBackTitle: " ",
        }} />
        <Stack.Screen name="ScanPalm" component={ScanPalm} options={{
          headerShown: true, title: 'Scan your palm',
          headerStyle: {
            backgroundColor: '#171531', elevation: 0,
            shadowColor: 'transparent'
          },
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
          headerBackTitle: " ",
          headerRight: () => (
            <Button
              onPress={() => { }}
              title="SKIP"
              color="gray"
            />
          ),
        }} />
        <Stack.Screen name="HoroscopeDetail" component={HoroscopeDetail} options={({ navigation }) => ({
          headerShown: true, title: 'Today, 18 December Friday',
          headerStyle: {
            backgroundColor: '#171531', elevation: 0,
            shadowColor: 'transparent'
          },
          headerTitleStyle: { color: '#5468ff' },
          headerTintColor: { color: '#5468ff' },
          headerBackTitle: " ",
          headerRight: () => (
            <Button
              onPress={() => {navigation.navigate("ScanPalm")}}
              title="SCAN"
              color="gray"
            />
          ),
        })} />
        <Stack.Screen name="OpenCamera" component={OpenCamera} options={{
          title: 'Left hand',
          headerTransparent: true,
          headerBackTitle: " ",
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
        }} />

        <Stack.Screen name="SendPalm" component={SendPalm} options={{
          title: 'Send your palm',
          headerTransparent: true,
          headerBackTitle: " ",
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
        }} />
        <Stack.Screen name="Preparing" component={Preparing} options={{
          headerTransparent: true,
          headerBackTitle: " ",
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
        }} />
        <Stack.Screen name="Analyzing" component={Analyzing} options={{
          headerTransparent: true,
          headerBackTitle: " ",
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
        }} />
        <Stack.Screen name="Notifications" component={Notifications} options={{
          headerTransparent: true,
          headerBackTitle: " ",
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
        }} />
        <Stack.Screen name="PalmAnalyzing" component={PalmAnalyzing} options={({ navigation }) => ({
          headerTransparent: true,
          headerBackTitle: " ",
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
          headerTitle: " ",
          headerRight: () => (
            <Button
              onPress={() => {navigation.navigate("Analysis")}}
              title="Analysis"
              color="black"
            />
          ),
        })}/>
        <Stack.Screen name="Analysis" component={Analysis} options={{
          headerTransparent: true,
          headerBackTitle: " ",
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
        }} />
        <Stack.Screen name="Palmistry" component={Palmistry} options={{
          headerTransparent: true,
          headerBackTitle: " ",
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
        }} />
       <Stack.Screen name="Settings" component={Settings} options={{
          headerTransparent: true,
          headerBackTitle: " ",
          headerTitleStyle: { color: 'white' },
          headerTintColor: { color: 'white' },
        }} />
        <Stack.Screen name="Privacy" component={Privacy} options={{
           headerTransparent: true,
           headerBackTitle: " ",
           headerTitleStyle: { color: 'white' },
           headerTintColor: { color: 'white' },
         }} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
/* 
const SignedIn = createAppContainer(createBottomTabNavigator(
  {
    Home: { screen: HomeStack },
    PaidBills: { screen: PaidBillsStack },
    PayBill: { screen: PayBillStack },
    Outgoings: { screen: OutgoingsStack },
    Profile:{ screen: ProfileStack },
  },
  { 
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = focused ? 'homeYellow' : 'home';
        } else if (routeName === 'PaidBills') {
          iconName = focused ? 'faturalar-mYellow' : 'faturalar-m';
        } else if (routeName === 'PayBill') {
          iconName = focused ? 'groupYellow' : 'group';
        } else if (routeName === 'Outgoings') {
          iconName = focused ? 'harcamalar-mYellow' : 'harcamalar-m';
        } else if (routeName === 'Profile') {
          iconName = focused ? 'profileYellow' : 'profile';
        }
        console.log("iconname",iconName)
        return <SafeAreaView><CustomIcons icon={iconName} size={scale(40)}/></SafeAreaView>;
      }
    }),
    tabBarOptions: {
      showLabel: false,
      keyboardHidesTabBar: true,
      style: {
        backgroundColor: colors.headerColor,
      }
    }
  }
)); */

export default Router